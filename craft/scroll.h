#ifndef SCROLL_H
#define SCROLL_H

#include "witch.h"
#include <QTimer>
#include <QLabel>
#include <QResizeEvent>

#include <QScrollArea>

class Scroll: public QScrollArea
{
    Q_OBJECT

public:
    Scroll(Witch& witch, QWidget* parent = nullptr);

    void refresh();
    static QRect contentRect(const QSize& size);

    void setColor(QColor col) { pen_.setColor(col); }

protected:
    void resizeEvent(QResizeEvent* e) override;
    Broom broom() const;

private:
    void updateBackground();
    void drawCyclist(QImage& image);

    Witch& witch_;
    QLabel* urn_;
    QPainter scribe_;
    QPen pen_;
};

#endif // SCROLL_H
