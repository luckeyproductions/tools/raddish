#ifndef PRESETBUTTON_H
#define PRESETBUTTON_H

#include "rune.h"
#include "defs.h"

#include <QToolButton>

class PresetButton: public QToolButton
{
    Q_OBJECT

public:

    PresetButton(const QColor& color = Qt::red, QWidget* parent = nullptr);

    void setColor(const QColor& col) { strokeColor_ = col; }

signals:
    void picked(Rune r);

private slots:
    void pickRune(QAction* a);

private:
    QIcon icon(Preset preset);
    QColor strokeColor_;
};

#endif // PRESETBUTTON_H
