#ifndef DRAWIN_H
#define DRAWIN_H

#include "instrument.h"

class DrawIn: public Instrument
{
public:
    DrawIn(): Instrument(DRAW) {}

    void strum(const QLineF& at) override;

private:
    Witch* draft_;
};

#endif // DRAWIN_H
