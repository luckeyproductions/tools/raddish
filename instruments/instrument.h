#ifndef TOOLS_H
#define TOOLS_H

#include "defs.h"
#include "witch.h"
#include <unordered_map>

#include <QObject>
#include <QAction>

class Instrument: public QObject
{
    Q_OBJECT
protected:
    Instrument(Tool t);
    std::vector<QAction*> operations_;

public:
    static Instrument* held() { return held_; }
    static Instrument* grab(Tool t, bool hold = true);
    static Instrument* grab(QObject* action);
    static std::vector<Instrument*> all();
    static std::vector<QAction*> actions(Tool t = ZEBRA);

    operator Tool     () const { return type_  ; }
    operator QAction* () const { return action_; }
    operator QString  () const {
        switch (type_) {
        case MOVE: return { "Move" };
        case DRAW: return { "Draw" };
        default:   return {};
        }
    }

    QAction* action() const { return action_; }
    virtual void  pick(const QPointF& at) { /*select*/ }
    virtual void strum(const QLineF&  at) {}

    std::vector<QAction*> operations() const { return operations_; };

private:
    static std::unordered_map<Tool, Instrument*> instrumentarium_;
    static Instrument* held_;

    Tool type_;
    QAction* action_;
};

#endif // TOOLS_H
