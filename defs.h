#ifndef RADDISHDEFS_H
#define RADDISHDEFS_H

#include "luckeymath.h"
#include <QString>

enum Preset{ LINE, SEMICIRCLE, CORNER, RANDOM, NUM_PRESETS };
enum   Tool{ MOVE, DRAW, ZEBRA };

struct AngleName: public QString
{
    AngleName(AngleUnit u): QString()
    {
        switch (u) {
        case TAU_RAD: append("Turns"); break;
        case PI_RAD:  append("Pi Radians" ); break;
        case RAD:     append("Radians"    ); break;
        case DEG:     append("Degrees"    ); break;
        case GON:     append("Gons"       ); break;
        case QARC:    append("Qt Arc"     ); break;
        case SPOKE:   append("Spokes"     ); break;
        }
    }
};

#endif // RADDISHDEFS_H
