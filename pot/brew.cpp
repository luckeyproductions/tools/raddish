#include "pot.h"
#include "cook.h"

#include "brew.h"

Brew::Brew(Pot* pot): QImage(pot->size(),
              QImage::Format_RGBA8888),
    pot_{ pot },
    mass_{}
{
    fill(0);
}

Brew::Brew(const QSize& size, Pot* pot): QImage(size,
                QImage::Format_RGBA8888),
    pot_{ pot },
    mass_{}
{
    fill(0);
}

Brew::Brew(const Brew* brew): QImage(brew->convertToFormat(
                         QImage::Format_RGBA8888)),
    pot_{ brew->pot_ },
    mass_{ brew->mass_ }
{
}

void Brew::redraw(const QRect& rect)
{
    const Brew stag{ stagnant(rect) };
    fill(0);

    Cook c{ this, SIZZLER };
    c.drawImage(QPoint{}, stag);
    c.drawImage(rect, novel(rect));
    c.end();
}

Brew Brew::novel(const QRect& rect)
{
    Brew nova{ rect.size(), pot_ };
    Cook c{ &nova, PARTIE };
    c.offset(-rect.topLeft());

    const int hue{ 0400 };

    for (const Witch& w: mass_)
    {
        if (pot_->view().map(w.bounds()).intersects(rect))
        {
            c.strokePath(w, { Qt::black, 2.3, Qt::SolidLine, Qt::FlatCap });
            c.strokePath(w, { QColor::fromHsv(hue, 0300, 255), 1.7, Qt::SolidLine, Qt::RoundCap });
        }
    }

    c.end();
    return nova;
}

Brew Brew::stagnant(const QRect& rect)
{
    Brew stag{ pot_ };
    QPainter p{ &stag };

    for (int q{0}; q < 4; ++q)
    {
        QPoint topLeft{};
        QSize quadrantSize{};

        switch (q)
        { default:
        case 0:
            topLeft = QPoint{};
            quadrantSize = QSize{ rect.left(), height() };
        break;
        case 1:
            topLeft = QPoint{ rect.right(), 0 };
            quadrantSize = QSize{ width() - rect.right(), height() };
        break;
        case 2:
            topLeft = QPoint{ rect.left(), 0 };
            quadrantSize = QSize{ rect.width(), rect.top() };
        break;
        case 3:
            topLeft = rect.bottomLeft();
            quadrantSize = QSize{ rect.width(), height() - rect.bottom() };
        break;
        }

        QRect quadrant{ topLeft, quadrantSize };
        p.drawImage(topLeft, copy(quadrant));
    }

    p.end();
    return stag;
}

void Brew::resizzle()
{
    Cook::resizzle(*this);
}

void Brew::clear()
{
    mass_.clear();
    fill(0);
}
