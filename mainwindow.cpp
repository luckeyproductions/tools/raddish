#include "icons.h"
#include <QGuiApplication>
#include <QScreen>
#include <QComboBox>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget* parent): QMainWindow(parent),
    crafts_{},
    pot_{ new Pot{} }
{
    std::locale::global(std::locale::classic());

    const QString displayName{ "Raddish" };
    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckeyprodutions.nl");
    QCoreApplication::setApplicationName(displayName.toLower());
    QGuiApplication::setApplicationDisplayName(displayName);

    setWindowIcon(QIcon{ ":/Raddish" });
    setWindowTitle(displayName);
    createToolBar();
    setCentralWidget(pot_);

    QSize screenSize{ QGuiApplication::primaryScreen()->size() };
    setGeometry(Scroll::contentRect(screenSize));

    createCrafts();

    Instrument::grab(DRAW);
}

void MainWindow::createToolBar()
{
    QToolBar* toolBar{ new QToolBar{} };
    toolBar->setWindowTitle("Band");

    QComboBox* unitBox{ new QComboBox{} };

    for (AngleUnit u{ SPOKE }; u < QARC;
         u = static_cast<AngleUnit>(u + 1))
    {
        unitBox->addItem(AngleName{ u });
    }

    toolBar->addWidget(unitBox);

    toolBar->addAction(createTool(MOVE));
    toolBar->addAction(createTool(DRAW));

    addToolBar(toolBar);
}

QAction* MainWindow::createTool(Tool t)
{
    QAction* tool{ *Instrument::grab(t, false) };
    connect(tool, SIGNAL(triggered()), this, SLOT(toolTriggered()));

    return tool;
}

void MainWindow::toolTriggered()
{
    Instrument::grab(sender());
}

void MainWindow::createCrafts()
{
    for (int c{0}; c < 8;)
    {
        if (c == 6)
        {
            ActiveCraft* active{ new ActiveCraft{} };
            createDockWidget(active, Qt::BottomDockWidgetArea)
                    ->setWindowTitle("Active");

            connect(pot_, SIGNAL(activeChanged(Witch*)), active, SLOT(setActive(Witch*)));
        }

        Craft* craft{ new Craft{} };
        (c < 4 ? createDockWidget(craft, c / 2 ? Qt::RightDockWidgetArea
                                               : Qt::LeftDockWidgetArea)
               : createDockWidget(craft, Qt::BottomDockWidgetArea))

                ->setWindowTitle("Craft " + QString::number(++c));

        crafts_.push_back(craft);
    }

    show();

    for (Craft* c: crafts_)
        c->resize( QSize((width()  -  011) / 4,
                         (height() - 0220) / 3));
}
